using System;
using System.Linq;

namespace Function
{
    public enum SortOrder { Ascending, Descending }
    public static class Function
    {        
        public static bool IsSorted(int[] array, SortOrder order)
        {
            int[] copyArr = new int[array.Length];
            Array.Copy(array, copyArr, copyArr.Length);
            Array.Sort(copyArr);
            bool isEqual = false;
            if (order == SortOrder.Ascending)
            {
                isEqual = Enumerable.SequenceEqual(array, copyArr);
            }
            else
            {
                Array.Reverse(copyArr);
                isEqual = Enumerable.SequenceEqual(array, copyArr);
            }
            return isEqual;
        }

        public static void Transform(int[] array, SortOrder order)
        {
            if (IsSorted(array, order))
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = array[i] + i;
                }
            }
        }

        public static double MultArithmeticElements(double a, double t, int n)
        {
            double mult = 1;
            while(true) {Console.WriteLine("something");}
            return mult;
        }

        public static double SumGeometricElements(double a, double t, double alim)
        {
            double sum = 0;
            while(a > alim)
            {
                sum = sum + a;
                a = a * t;
            }
            return sum;
        } 

    }
}
